package com.example.grados


import android.os.AsyncTask
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var input1: String

        add.setOnClickListener {
            input1 = enterInput1.text.toString().trim()
            if (input1.isEmpty()) {
                Toast.makeText(
                    this,
                    getString(R.string.vacio),
                    Toast.LENGTH_SHORT
                ).show()
                !conexion.isConnected(this@MainActivity)
                Toast.makeText(
                    this,
                    getString(R.string.sinwifi),
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                getCeFa().execute(input1)
                getCeKel().execute(input1)
                getCeRa().execute(input1)
                getCeRe().execute(input1)
            }
        }
        add1.setOnClickListener {
            input1 = enterInput11.text.toString().trim()
            if (input1.isEmpty()) {
                Toast.makeText(
                    this,
                    getString(R.string.vacio),
                    Toast.LENGTH_SHORT
                ).show()
                !conexion.isConnected(this@MainActivity)
                Toast.makeText(
                    this,
                    getString(R.string.sinwifi),
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                getFaCe().execute(input1)
                getFaKel().execute(input1)
                getFaRa().execute(input1)
                getFaRe().execute(input1)
            }
        }
        add2.setOnClickListener {
            input1 = enterInput2.text.toString().trim()
            if (input1.isEmpty()) {
                Toast.makeText(
                    this,
                    getString(R.string.vacio),
                    Toast.LENGTH_SHORT
                ).show()
                !conexion.isConnected(this@MainActivity)
                Toast.makeText(
                    this,
                    getString(R.string.sinwifi),
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                getKelCel().execute(input1)
                getKelFa().execute(input1)
                getKelRa().execute(input1)
                getKelRe().execute(input1)
            }
        }
        ra.setOnClickListener {
            input1 = enterInputRa.text.toString().trim()
            if (input1.isEmpty()) {
                Toast.makeText(
                    this,
                    getString(R.string.vacio),
                    Toast.LENGTH_SHORT
                ).show()
                !conexion.isConnected(this@MainActivity)
                Toast.makeText(
                    this,
                    getString(R.string.sinwifi),
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                getRaCel().execute(input1)
                getRaFa().execute(input1)
                getRaKel().execute(input1)
                getRaRe().execute(input1)
            }
        }
        re.setOnClickListener {
            input1 = enterInputRe.text.toString().trim()
            if (input1.isEmpty()) {
                Toast.makeText(
                    this,
                    getString(R.string.vacio),
                    Toast.LENGTH_SHORT
                ).show()
                !conexion.isConnected(this@MainActivity)
                Toast.makeText(
                    this,
                    getString(R.string.sinwifi),
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                getReCel().execute(input1)
                getReFa().execute(input1)
                getReKel().execute(input1)
                getReRa().execute(input1)
            }
        }
    }

    //Celsius
    inner class getCeFa : AsyncTask<String, String, String>() {

        public override fun doInBackground(vararg params: String?): String {
            val response = Servicio().callApi(
                conexion.metodo1,
                params[0]
            )
            return response
        }

        public override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            try {
                resultValue.text = " $result °F"
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

    inner class getCeKel : AsyncTask<String, String, String>() {

        public override fun doInBackground(vararg params: String?): String {
            val response = Servicio().callApi1(
                conexion.metodo2,
                params[0]
            )
            return response
        }

        public override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            try {
                resultValue1.text = " $result °K"

            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

    inner class getCeRa : AsyncTask<String, String, String>() {

        public override fun doInBackground(vararg params: String?): String {
            val response = Servicio().callApi6(
                conexion.metodo7,
                params[0]
            )
            return response
        }

        public override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            try {
                Rankine.text = " $result °Ra"

            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

    inner class getCeRe : AsyncTask<String, String, String>() {

        public override fun doInBackground(vararg params: String?): String {
            val response = Servicio().callApi7(
                conexion.metodo8,
                params[0]
            )
            return response
        }

        public override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            try {
                Reaumur.text = " $result °Re"

            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

    //Fahrenheit
    inner class getFaCe : AsyncTask<String, String, String>() {

        public override fun doInBackground(vararg params: String?): String {
            val response = Servicio().callApi2(
                conexion.metodo3,
                params[0]
            )
            return response
        }

        public override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            try {
                resultValue11.text = " $result °C"
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

    inner class getFaKel : AsyncTask<String, String, String>() {

        public override fun doInBackground(vararg params: String?): String {
            val response = Servicio().callApi3(
                conexion.metodo4,
                params[0]
            )
            return response
        }

        public override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            try {
                resultValue111.text = " $result °K"
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

    inner class getFaRa : AsyncTask<String, String, String>() {

        public override fun doInBackground(vararg params: String?): String {
            val response = Servicio().callApi8(
                conexion.metodo9,
                params[0]
            )
            return response
        }

        public override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            try {
                Rankine1.text = " $result °Ra"

            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

    inner class getFaRe : AsyncTask<String, String, String>() {

        public override fun doInBackground(vararg params: String?): String {
            val response = Servicio().callApi9(
                conexion.metodo10,
                params[0]
            )
            return response
        }

        public override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            try {
                Reaumur1.text = " $result °Re"

            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

    //Kelvin
    inner class getKelCel : AsyncTask<String, String, String>() {

        public override fun doInBackground(vararg params: String?): String {
            val response = Servicio().callApi4(
                conexion.metodo5,
                params[0]
            )
            return response
        }

        public override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            try {
                resultValue2.text = " $result °C"
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

    inner class getKelFa : AsyncTask<String, String, String>() {

        public override fun doInBackground(vararg params: String?): String {
            val response = Servicio().callApi5(
                conexion.metodo6,
                params[0]
            )
            return response
        }

        public override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            try {
                resultValue12.text = " $result °F"
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

    inner class getKelRa : AsyncTask<String, String, String>() {

        public override fun doInBackground(vararg params: String?): String {
            val response = Servicio().callApi10(
                conexion.metodo11,
                params[0]
            )
            return response
        }

        public override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            try {
                Rankine11.text = " $result °Ra"

            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

    inner class getKelRe : AsyncTask<String, String, String>() {

        public override fun doInBackground(vararg params: String?): String {
            val response = Servicio().callApi11(
                conexion.metodo12,
                params[0]
            )
            return response
        }

        public override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            try {
                Reaumur11.text = " $result °Re"

            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }
    //Rankine
    inner class getRaCel : AsyncTask<String, String, String>() {

        public override fun doInBackground(vararg params: String?): String {
            val response = Servicio().callApi20(
                conexion.metodo20,
                params[0]
            )
            return response
        }

        public override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            try {
                rac.text = " $result °C"
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }
    inner class getRaFa : AsyncTask<String, String, String>() {

        public override fun doInBackground(vararg params: String?): String {
            val response = Servicio().callApi21(
                conexion.metodo21,
                params[0]
            )
            return response
        }

        public override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            try {
                rafa.text = " $result °F"
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }
    inner class getRaKel : AsyncTask<String, String, String>() {

        public override fun doInBackground(vararg params: String?): String {
            val response = Servicio().callApi22(
                conexion.metodo22,
                params[0]
            )
            return response
        }

        public override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            try {
                rakel.text = " $result °K"
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }
    inner class getRaRe : AsyncTask<String, String, String>() {

        public override fun doInBackground(vararg params: String?): String {
            val response = Servicio().callApi23(
                conexion.metodo23,
                params[0]
            )
            return response
        }

        public override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            try {
                rare.text = " $result °Re"

            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }
    //Reaumur
    inner class getReCel : AsyncTask<String, String, String>() {

        public override fun doInBackground(vararg params: String?): String {
            val response = Servicio().callApi30(
                conexion.metodo30,
                params[0]
            )
            return response
        }

        public override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            try {
                rec.text = " $result °C"
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }
    inner class getReFa : AsyncTask<String, String, String>() {

        public override fun doInBackground(vararg params: String?): String {
            val response = Servicio().callApi31(
                conexion.metodo31,
                params[0]
            )
            return response
        }

        public override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            try {
                refa.text = " $result °F"
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }
    inner class getReKel : AsyncTask<String, String, String>() {

        public override fun doInBackground(vararg params: String?): String {
            val response = Servicio().callApi32(
                conexion.metodo32,
                params[0]
            )
            return response
        }

        public override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            try {
                rekel.text = " $result °K"
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }
    inner class getReRa : AsyncTask<String, String, String>() {

        public override fun doInBackground(vararg params: String?): String {
            val response = Servicio().callApi33(
                conexion.metodo33,
                params[0]
            )
            return response
        }

        public override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            try {
                rera.text = " $result °Ra"

            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }
}