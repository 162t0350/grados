package com.example.grados

import org.ksoap2.SoapEnvelope
import org.ksoap2.serialization.SoapObject
import org.ksoap2.serialization.SoapSerializationEnvelope
import org.ksoap2.transport.HttpTransportSE

class Servicio {
    fun callApi(
        methodName: String,
        input1: String?,

        ): String {
        var result = ""
        val soapaction = "http://Convertir1.somee.com/CeFa"
        val soapObject = SoapObject(conexion.SOAP_NAMESPACE, methodName)


        soapObject.addProperty("dato1", input1)

        val envelope = SoapSerializationEnvelope(SoapEnvelope.VER11)
        envelope.setOutputSoapObject(soapObject)

        envelope.dotNet = true


        val httpTransportSE = HttpTransportSE(conexion.SOAP_URL)

        return try {
            httpTransportSE.call(soapaction, envelope)
            val soapPrimitive = envelope.response
            result = soapPrimitive.toString()
            result

        } catch (e: Exception) {
            e.printStackTrace()
            "Error!"
        }
        return result
    }

    fun callApi1(
        methodName: String,
        input1: String?,

        ): String {
        var result = ""
        val soapaction = "http://Convertir1.somee.com/CeKel"
        val soapObject = SoapObject(conexion.SOAP_NAMESPACE, methodName)


        soapObject.addProperty("dato1", input1)

        val envelope = SoapSerializationEnvelope(SoapEnvelope.VER11)
        envelope.setOutputSoapObject(soapObject)

        envelope.dotNet = true


        val httpTransportSE = HttpTransportSE(conexion.SOAP_URL)

        return try {
            httpTransportSE.call(soapaction, envelope)
            val soapPrimitive = envelope.response
            result = soapPrimitive.toString()
            result

        } catch (e: Exception) {
            e.printStackTrace()
            "Error!"
        }
        return result
    }
    fun callApi2(
        methodName: String,
        input1: String?,

        ): String {
        var result = ""
        val soapaction = "http://Convertir1.somee.com/FaCe"
        val soapObject = SoapObject(conexion.SOAP_NAMESPACE, methodName)


        soapObject.addProperty("dato1", input1)

        val envelope = SoapSerializationEnvelope(SoapEnvelope.VER11)
        envelope.setOutputSoapObject(soapObject)

        envelope.dotNet = true


        val httpTransportSE = HttpTransportSE(conexion.SOAP_URL)

        return try {
            httpTransportSE.call(soapaction, envelope)
            val soapPrimitive = envelope.response
            result = soapPrimitive.toString()
            result

        } catch (e: Exception) {
            e.printStackTrace()
            "Error!"
        }
        return result
    }
    fun callApi3(
        methodName: String,
        input1: String?,

        ): String {
        var result = ""
        val soapaction = "http://Convertir1.somee.com/FaKel"
        val soapObject = SoapObject(conexion.SOAP_NAMESPACE, methodName)


        soapObject.addProperty("dato1", input1)

        val envelope = SoapSerializationEnvelope(SoapEnvelope.VER11)
        envelope.setOutputSoapObject(soapObject)

        envelope.dotNet = true


        val httpTransportSE = HttpTransportSE(conexion.SOAP_URL)

        return try {
            httpTransportSE.call(soapaction, envelope)
            val soapPrimitive = envelope.response
            result = soapPrimitive.toString()
            result

        } catch (e: Exception) {
            e.printStackTrace()
            "Error!"
        }
        return result
    }
    fun callApi4(
        methodName: String,
        input1: String?,

        ): String {
        var result = ""
        val soapaction = "http://Convertir1.somee.com/KelCel"
        val soapObject = SoapObject(conexion.SOAP_NAMESPACE, methodName)


        soapObject.addProperty("dato1", input1)

        val envelope = SoapSerializationEnvelope(SoapEnvelope.VER11)
        envelope.setOutputSoapObject(soapObject)

        envelope.dotNet = true


        val httpTransportSE = HttpTransportSE(conexion.SOAP_URL)

        return try {
            httpTransportSE.call(soapaction, envelope)
            val soapPrimitive = envelope.response
            result = soapPrimitive.toString()
            result

        } catch (e: Exception) {
            e.printStackTrace()
            "Error!"
        }
        return result
    }
    fun callApi5(
        methodName: String,
        input1: String?,

        ): String {
        var result = ""
        val soapaction = "http://Convertir1.somee.com/KelFa"
        val soapObject = SoapObject(conexion.SOAP_NAMESPACE, methodName)


        soapObject.addProperty("dato1", input1)

        val envelope = SoapSerializationEnvelope(SoapEnvelope.VER11)
        envelope.setOutputSoapObject(soapObject)

        envelope.dotNet = true


        val httpTransportSE = HttpTransportSE(conexion.SOAP_URL)

        return try {
            httpTransportSE.call(soapaction, envelope)
            val soapPrimitive = envelope.response
            result = soapPrimitive.toString()
            result

        } catch (e: Exception) {
            e.printStackTrace()
            "Error!"
        }
        return result
    }
    fun callApi6(
        methodName: String,
        input1: String?,

        ): String {
        var result = ""
        val soapaction = "http://Convertir1.somee.com/CeRa"
        val soapObject = SoapObject(conexion.SOAP_NAMESPACE, methodName)


        soapObject.addProperty("dato1", input1)

        val envelope = SoapSerializationEnvelope(SoapEnvelope.VER11)
        envelope.setOutputSoapObject(soapObject)

        envelope.dotNet = true


        val httpTransportSE = HttpTransportSE(conexion.SOAP_URL)

        return try {
            httpTransportSE.call(soapaction, envelope)
            val soapPrimitive = envelope.response
            result = soapPrimitive.toString()
            result

        } catch (e: Exception) {
            e.printStackTrace()
            "Error!"
        }
        return result
    }
    fun callApi7(
        methodName: String,
        input1: String?,

        ): String {
        var result = ""
        val soapaction = "http://Convertir1.somee.com/CeRe"
        val soapObject = SoapObject(conexion.SOAP_NAMESPACE, methodName)


        soapObject.addProperty("dato1", input1)

        val envelope = SoapSerializationEnvelope(SoapEnvelope.VER11)
        envelope.setOutputSoapObject(soapObject)

        envelope.dotNet = true


        val httpTransportSE = HttpTransportSE(conexion.SOAP_URL)

        return try {
            httpTransportSE.call(soapaction, envelope)
            val soapPrimitive = envelope.response
            result = soapPrimitive.toString()
            result

        } catch (e: Exception) {
            e.printStackTrace()
            "Error!"
        }
        return result
    }
    fun callApi8(
        methodName: String,
        input1: String?,

        ): String {
        var result = ""
        val soapaction = "http://Convertir1.somee.com/FaRa"
        val soapObject = SoapObject(conexion.SOAP_NAMESPACE, methodName)


        soapObject.addProperty("dato1", input1)

        val envelope = SoapSerializationEnvelope(SoapEnvelope.VER11)
        envelope.setOutputSoapObject(soapObject)

        envelope.dotNet = true


        val httpTransportSE = HttpTransportSE(conexion.SOAP_URL)

        return try {
            httpTransportSE.call(soapaction, envelope)
            val soapPrimitive = envelope.response
            result = soapPrimitive.toString()
            result

        } catch (e: Exception) {
            e.printStackTrace()
            "Error!"
        }
        return result
    }
    fun callApi9(
        methodName: String,
        input1: String?,

        ): String {
        var result = ""
        val soapaction = "http://Convertir1.somee.com/FaRe"
        val soapObject = SoapObject(conexion.SOAP_NAMESPACE, methodName)


        soapObject.addProperty("dato1", input1)

        val envelope = SoapSerializationEnvelope(SoapEnvelope.VER11)
        envelope.setOutputSoapObject(soapObject)

        envelope.dotNet = true


        val httpTransportSE = HttpTransportSE(conexion.SOAP_URL)

        return try {
            httpTransportSE.call(soapaction, envelope)
            val soapPrimitive = envelope.response
            result = soapPrimitive.toString()
            result

        } catch (e: Exception) {
            e.printStackTrace()
            "Error!"
        }
        return result
    }
    fun callApi10(
        methodName: String,
        input1: String?,

        ): String {
        var result = ""
        val soapaction = "http://Convertir1.somee.com/KelRa"
        val soapObject = SoapObject(conexion.SOAP_NAMESPACE, methodName)


        soapObject.addProperty("dato1", input1)

        val envelope = SoapSerializationEnvelope(SoapEnvelope.VER11)
        envelope.setOutputSoapObject(soapObject)

        envelope.dotNet = true


        val httpTransportSE = HttpTransportSE(conexion.SOAP_URL)

        return try {
            httpTransportSE.call(soapaction, envelope)
            val soapPrimitive = envelope.response
            result = soapPrimitive.toString()
            result

        } catch (e: Exception) {
            e.printStackTrace()
            "Error!"
        }
        return result
    }
    fun callApi11(
        methodName: String,
        input1: String?,

        ): String {
        var result = ""
        val soapaction = "http://Convertir1.somee.com/KelRe"
        val soapObject = SoapObject(conexion.SOAP_NAMESPACE, methodName)


        soapObject.addProperty("dato1", input1)

        val envelope = SoapSerializationEnvelope(SoapEnvelope.VER11)
        envelope.setOutputSoapObject(soapObject)

        envelope.dotNet = true


        val httpTransportSE = HttpTransportSE(conexion.SOAP_URL)

        return try {
            httpTransportSE.call(soapaction, envelope)
            val soapPrimitive = envelope.response
            result = soapPrimitive.toString()
            result

        } catch (e: Exception) {
            e.printStackTrace()
            "Error!"
        }
        return result
    }

    //Parte Temperaturas2
    fun callApi20(
        methodName: String,
        input1: String?,

        ): String {
        var result = ""
        val soapaction = "http://Convertir1.somee.com/RaCe"
        val soapObject = SoapObject(conexion.SOAP_NAMESPACE, methodName)


        soapObject.addProperty("dato1", input1)

        val envelope = SoapSerializationEnvelope(SoapEnvelope.VER11)
        envelope.setOutputSoapObject(soapObject)

        envelope.dotNet = true


        val httpTransportSE = HttpTransportSE(conexion.SOAP_URL)

        return try {
            httpTransportSE.call(soapaction, envelope)
            val soapPrimitive = envelope.response
            result = soapPrimitive.toString()
            result

        } catch (e: Exception) {
            e.printStackTrace()
            "Error!"
        }
        return result
    }
    fun callApi21(
        methodName: String,
        input1: String?,

        ): String {
        var result = ""
        val soapaction = "http://Convertir1.somee.com/RaFa"
        val soapObject = SoapObject(conexion.SOAP_NAMESPACE, methodName)


        soapObject.addProperty("dato1", input1)

        val envelope = SoapSerializationEnvelope(SoapEnvelope.VER11)
        envelope.setOutputSoapObject(soapObject)

        envelope.dotNet = true


        val httpTransportSE = HttpTransportSE(conexion.SOAP_URL)

        return try {
            httpTransportSE.call(soapaction, envelope)
            val soapPrimitive = envelope.response
            result = soapPrimitive.toString()
            result

        } catch (e: Exception) {
            e.printStackTrace()
            "Error!"
        }
        return result
    }
    fun callApi22(
        methodName: String,
        input1: String?,

        ): String {
        var result = ""
        val soapaction = "http://Convertir1.somee.com/RaKel"
        val soapObject = SoapObject(conexion.SOAP_NAMESPACE, methodName)


        soapObject.addProperty("dato1", input1)

        val envelope = SoapSerializationEnvelope(SoapEnvelope.VER11)
        envelope.setOutputSoapObject(soapObject)

        envelope.dotNet = true


        val httpTransportSE = HttpTransportSE(conexion.SOAP_URL)

        return try {
            httpTransportSE.call(soapaction, envelope)
            val soapPrimitive = envelope.response
            result = soapPrimitive.toString()
            result

        } catch (e: Exception) {
            e.printStackTrace()
            "Error!"
        }
        return result
    }
    fun callApi23(
        methodName: String,
        input1: String?,

        ): String {
        var result = ""
        val soapaction = "http://Convertir1.somee.com/RaRe"
        val soapObject = SoapObject(conexion.SOAP_NAMESPACE, methodName)


        soapObject.addProperty("dato1", input1)

        val envelope = SoapSerializationEnvelope(SoapEnvelope.VER11)
        envelope.setOutputSoapObject(soapObject)

        envelope.dotNet = true


        val httpTransportSE = HttpTransportSE(conexion.SOAP_URL)

        return try {
            httpTransportSE.call(soapaction, envelope)
            val soapPrimitive = envelope.response
            result = soapPrimitive.toString()
            result

        } catch (e: Exception) {
            e.printStackTrace()
            "Error!"
        }
        return result
    }

    //Parte Temperaturas3
    fun callApi30(
        methodName: String,
        input1: String?,

        ): String {
        var result = ""
        val soapaction = "http://Convertir1.somee.com/ReCe"
        val soapObject = SoapObject(conexion.SOAP_NAMESPACE, methodName)


        soapObject.addProperty("dato1", input1)

        val envelope = SoapSerializationEnvelope(SoapEnvelope.VER11)
        envelope.setOutputSoapObject(soapObject)

        envelope.dotNet = true


        val httpTransportSE = HttpTransportSE(conexion.SOAP_URL)

        return try {
            httpTransportSE.call(soapaction, envelope)
            val soapPrimitive = envelope.response
            result = soapPrimitive.toString()
            result

        } catch (e: Exception) {
            e.printStackTrace()
            "Error!"
        }
        return result
    }
    fun callApi31(
        methodName: String,
        input1: String?,

        ): String {
        var result = ""
        val soapaction = "http://Convertir1.somee.com/ReFa"
        val soapObject = SoapObject(conexion.SOAP_NAMESPACE, methodName)


        soapObject.addProperty("dato1", input1)

        val envelope = SoapSerializationEnvelope(SoapEnvelope.VER11)
        envelope.setOutputSoapObject(soapObject)

        envelope.dotNet = true


        val httpTransportSE = HttpTransportSE(conexion.SOAP_URL)

        return try {
            httpTransportSE.call(soapaction, envelope)
            val soapPrimitive = envelope.response
            result = soapPrimitive.toString()
            result

        } catch (e: Exception) {
            e.printStackTrace()
            "Error!"
        }
        return result
    }
    fun callApi32(
        methodName: String,
        input1: String?,

        ): String {
        var result = ""
        val soapaction = "http://Convertir1.somee.com/ReKel"
        val soapObject = SoapObject(conexion.SOAP_NAMESPACE, methodName)


        soapObject.addProperty("dato1", input1)

        val envelope = SoapSerializationEnvelope(SoapEnvelope.VER11)
        envelope.setOutputSoapObject(soapObject)

        envelope.dotNet = true


        val httpTransportSE = HttpTransportSE(conexion.SOAP_URL)

        return try {
            httpTransportSE.call(soapaction, envelope)
            val soapPrimitive = envelope.response
            result = soapPrimitive.toString()
            result

        } catch (e: Exception) {
            e.printStackTrace()
            "Error!"
        }
        return result
    }
    fun callApi33(
        methodName: String,
        input1: String?,

        ): String {
        var result = ""
        val soapaction = "http://Convertir1.somee.com/ReRa"
        val soapObject = SoapObject(conexion.SOAP_NAMESPACE, methodName)


        soapObject.addProperty("dato1", input1)

        val envelope = SoapSerializationEnvelope(SoapEnvelope.VER11)
        envelope.setOutputSoapObject(soapObject)

        envelope.dotNet = true


        val httpTransportSE = HttpTransportSE(conexion.SOAP_URL)

        return try {
            httpTransportSE.call(soapaction, envelope)
            val soapPrimitive = envelope.response
            result = soapPrimitive.toString()
            result

        } catch (e: Exception) {
            e.printStackTrace()
            "Error!"
        }
        return result
    }
}