package com.example.grados


import android.content.Context
import android.net.ConnectivityManager

class conexion{
    companion object {

        const val SOAP_URL = "http://Convertir1.somee.com/Servicio.asmx?"
        const val SOAP_NAMESPACE = "http://Convertir1.somee.com"
        const val metodo1 = "CeFa"
        const val metodo2 = "CeKel"
        const val metodo7 = "CeRa"
        const val metodo8 = "CeRe"
        const val metodo3 = "FaCe"
        const val metodo4="FaKel"
        const val metodo9 = "FaRa"
        const val metodo10 = "FaRe"
        const val metodo5="KelCel"
        const val metodo6="KelFa"
        const val metodo11 = "KelRa"
        const val metodo12 = "KelRe"
        //Parte Temperaturas2
        const val metodo20= "RaCe"
        const val metodo21= "RaFa"
        const val metodo22= "RaKel"
        const val metodo23= "RaRe"
        //Parte Temperaturas3
        const val metodo30= "ReCe"
        const val metodo31= "ReFa"
        const val metodo32= "ReKel"
        const val metodo33= "ReRa"
        fun isConnected(context: Context): Boolean {
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork = cm.activeNetworkInfo
            return activeNetwork != null && activeNetwork.isConnectedOrConnecting
        }
    }
}